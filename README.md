# Honeyfund Code Audition

This was a very fun code challange and I hope you are able to find value in it. I followed the conventional `BEM` syntax for the SCSS and imported a few variables just as I would in the "real world".

*Muli is no longer a font supported by Google.* Instead I used a comparable font called Mulish

Thank you again and I hope we talk soon! 😊
